#!/usr/bin/perl -w

use Net::SSH2;

my $ssh2 = Net::SSH2->new();

# hostname of the server you are connecting to
$ssh2->connect('asdf') or die;

# username and password
$ssh2->auth_password('guest', 'guest') or die;

# run a program
my $chan = $ssh2->channel();
$chan->exec('touch test');

# print the contents of a file
my $sftp = $ssh2->sftp();
my $fh = $sftp->open('/etc/passwd') or die;
print $_ while <$fh>;
